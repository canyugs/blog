import React from 'react';
import mermaid from 'mermaid';

const Mermaid = ({ chart }) => {
  mermaid.initialize({})

  if (!chart) return null

  return (
    <div className="mermaid">
      {chart}
    </div>
  )
}

export default Mermaid;
import React from 'react';
import Mermaid from './Mermaid';


const Book = () => {
  return (
    <Mermaid chart={`
    graph LR
    subgraph main[書籍-有錢人想的跟你不一樣]

      subgraph process[實現程序]
      Thinking(想法) --> Feeling(感覺)
      Feeling --> Action(行動)
      Action --> Result(結果)
      end

      Result -->|影響| 你的人生

      subgraph change[改變的步驟]
      察覺 --> 理解
      理解 --> 劃清界線
      劃清界線 --> 提出宣言/重新設定
      end
    
      Mind -.->|影響| Blueprint(金錢藍圖)
      subgraph past[過去的經驗]
      lang(語言設定) -->|制約| Mind(心靈)
      model(模仿)　-->|制約| Mind
      event(特殊事件) -->|制約| Mind
      end
    
      Rich(17 種財富檔案) -->|改造| Blueprint
      Rich -->|改造| Mind
      Mind -.->|移除|不好的習慣
      Blueprint -.->|影響| Thinking
      Blueprint -.->|影響| Feeling
      Blueprint -.->|影響| Action
    
    end
    
    style main fill:white,stroke:black,stroke-width:2px
    style Rich fill:lightcoral
    style Blueprint fill:yellow;
  `}/>
  )
}

export default Book;
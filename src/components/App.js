import React from "react";
import Header from "./Header";
import Book from "./Charts/Book";

const App = ({ title }) => {
  return (
    <div className="app">
      <Header />
      <div className="container">
        <Book />
      </div>
    </div>
  )
}

export default App;
import React from "react";

const Header = () => {
  return (
    <div className="header">
      <div className="header__title">Blog</div>
      <div className="header__menu">
          <a href="#">Charts</a>
      </div>
    </div>
  )
}

export default Header;
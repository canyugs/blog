# Blog with React

## Setup

* install pkg.

```shell
yarn add react react-dom
yarn add --dev parcel-bundler @babel/preset-react @babel/preset-env
```

* path setup gitlab page.
  * `"homepage"` in package.json
  * using build option `--public-url` and `-d` in parcel build 

* add scripts in package.json.

## Feature

### mermaid support

* install package
  ```shell
  yarn add mermaid
  ```

* Usage
  ```js
  import mermaid from 'mermaid';

  const Mermaid = ({ chart }) => {
    mermaid.initialize({})

    if (!chart) return null

    return (
      <div className="mermaid">
        {chart}
      </div>
    )
  }
  ```